require('dotenv').config()
const config = {
  twitter: {
    consumerKey: process.env.consumerKey,
    consumerSecret: process.env.consumerSecret,
    accessTokenKey: process.env.accessTokenKey,
    accessTokenSecret: process.env.accessTokenSecret
  },
  username: '@showGenerator'
}

module.exports = config
