const fs = require('fs-extra')

class Completed {

  constructor(){
    //readfile
    this._list = this.getCompleted()
  }

  getCompleted(){
    try {
      let data = JSON.parse(fs.readFileSync('./output/completed.json'))
      return data
    }catch (err){
      console.log('file not set')
      return []
    }
  }

  get list(){
    return this._list
  }

  deleteItemfromList(id){
    //remove from array and file
  }
  checkifComplete(id){
    for(let x of this._list){
      if(x == id) return true
    }
    return false
  }

}

module.exports = new Completed()
