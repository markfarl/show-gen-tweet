class Lists {

    constructor(name, list = []) {
        //Read List from File
        this._name = name;
        this._tweetList = list;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    get tweetList() {
        return this._tweetList;
    }

    addItem(id, tweet, user, options, date) {
        this._tweetList.push({id, tweet, user, options, date});
    }

    removeItem(id){
      //removeItem byID
    }
    clearList(){
      this._tweetList = [];
    }
}

module.exports = new Lists();
