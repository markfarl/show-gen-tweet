var Twitter = require('twitter')
var completed = require('../lib/Completed')
var current = require('../lib/Lists')

class Twitgen{
  constructor(config){
    this.client = new Twitter({
      consumer_key: config.twitter.consumerKey,
      consumer_secret: config.twitter.consumerSecret,
      access_token_key: config.twitter.accessTokenKey,
      access_token_secret: config.twitter.accessTokenSecret
    })
    this.username = config.username
  }
  async updateList(){
    let response = await this.client.get('users/search', {q: this.username})
    //Parse results and return compare with current List
    response.forEach(x => {
      if(!completed.checkifComplete(x.status.id)) current.addItem(x.status.id, x.status.text, x.status.user)
    })

    return true
  }
  post(text, callback){
    return this.client.post('statuses/update', {status: text}, callback)
  }


}

module.exports = Twitgen
